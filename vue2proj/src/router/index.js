import Vue from 'vue'
import Router from 'vue-router'
import {routes} from './routers'

Vue.use(Router)

export default new Router({
  mode: "history", //设置路由模式
  linkActiveClass:"active",//路由链接激活class统一设置之后自定义的全部失效
  routes
})
