import Vue from "vue";
import Vuex from "vuex";
import {ADD_COUNT, ADD_COUNT_BY_DATA} from "./mutationTypes";


Vue.use(Vuex);
export default new Vuex.Store({
  state: {
    count: 2,
    per: 'abc',
    aaaaa: 'bcde',
    list: [1, 2, 3, 4]
  },
  getters: {
    initData: (state, getters) => {
      return getters.filterList.map(i => i * state.count)
    },
    filterList: state => state.list.filter(i => i > 2)
  },
  mutations: {
    addCount: state => state.count++,
    addCountByData0: function (state, payload) {
      state.count += payload.num
      state.count += payload.num2
    },
    //使用常量命名的mutations
    //[ADD_COUNT]: state => state.count++,
    [ADD_COUNT](state) {
      state.count++
    },
    [ADD_COUNT_BY_DATA](state, payload) {
      state.count += payload.num3
      state.count += payload.num4
    }
  },
  actions: {
    actAddCount: function (context) {
      //context相当于this.$store
      context.commit('addCount')
    },
    //使用解构函数
    actAddCount2({commit, state, getters}) {
      commit('addCount')
    },
    //使用解构函数 传递参数
    actAddCount3({commit, state, getters}, payload) {
      commit('addCountByData0', payload)
    },
  },
  modules: {
    ab: {
      state: {
        count: 3,
        listData: [{name: '赵六'}, {name: '张三'}]
      },
      getters: {
        addCount: state => state.count++,
        getAbCount: (state, getters) => {
          //state, getters 都是当前模块下的
          return state.count
        },
        getRootCount: (state, getters, rootState) => {
          //rootState 根store下的state
          return rootState.count
        }
      },
      mutations: {
        abAddCount: (state) => {
          state.count++
        }
      },
      actions: {
        abActAddCount: ({commit, state, getters}) => {
          // commit('addCount')
          commit('abAddCount')
        }
      }
    },
    de: {
      state: {},
      getters: {},
      mutations: {},
      actions: {}
    }
  }
})
