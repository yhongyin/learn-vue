import Home from "@/components/Home";
import RouterParams from "@/components/RouterParams";

export const routes = [
  {
    path: '/',
    name: '默认',
    redirect: '/home'
  },
  {
    path: '/home',
    name: 'home',
    component: Home,
    children:[
      {
        path: 'news',
        component: ()=> import('@/components/News'),
      }
    ]
  },
  {
    path: '/routerParams',
    name: 'routerParams',
    component: RouterParams,
    beforeEnter:(to,from,next)=>{ //单个路由守卫
      console.log('进入routerParams前..')
      next()
    }
  },
  {
    path: '/routerQuery',
    name: 'routerQuery',
    component: () => import("@/components/RouterQuery")//路由懒加载
  },
  {
    path: '/user/:id',
    name: 'user',
    component: () => import("../components/User")//路由懒加载
  },
  {
    path: '/testSlot',
    name: 'testSlot',
    component: () => import("../views/slot/TestSlot")//路由懒加载
  },
  {
    path: '/vuex',
    name: 'vuex',
    component: () => import("../views/testVuex/TestVuex")//路由懒加载
  },
  {
    path: '/promise',
    name: 'promise',
    component: () => import("../views/promise/TestPromise")//路由懒加载
  },
  {
    path: '/axios',
    name: 'axios',
    component: () => import("../views/axios/TestAxios")//路由懒加载
  }
]
