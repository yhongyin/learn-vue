import router from "./index";
//全局路由守卫
//前置守卫
router.beforeEach((to, from, next) => {
  console.info('路由跳转前执行...')
  next()
})

//后置守卫
router.afterEach((to, from) => {
  console.info('路由跳转后执行...')
})
